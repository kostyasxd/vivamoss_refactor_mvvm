﻿using System.IO;
using Xamarin.Forms;
using Vivamos;
using Windows.Storage;
using Vivamos.UWP;

[assembly: Dependency(typeof(FileHelper))]
namespace Vivamos.UWP
{
	public class FileHelper : IFileHelper
	{
		public string GetLocalFilePath(string filename)
		{
			return Path.Combine(ApplicationData.Current.LocalFolder.Path, filename);
		}
	}
}

﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using Vivamos;

namespace Vivamos.MigrationsHost.Migrations
{
    [DbContext(typeof(VivamosDatabase))]
    [Migration("20180320155956_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452");

            modelBuilder.Entity("Vivamos.TodoItem", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Done");

                    b.Property<string>("Name");

                    b.Property<string>("Notes");

                    b.HasKey("ID");

                    b.ToTable("TodoItems");
                });
#pragma warning restore 612, 618
        }
    }
}

﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Diagnostics;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Vivamos
{
	public class App : Application
	{
		static VivamosDatabase database;

		public App()
		{
			Resources = new ResourceDictionary();
			Resources.Add("primaryGreen", Color.FromHex("91CA47"));
			Resources.Add("primaryDarkGreen", Color.FromHex("6FA22E"));

			var nav = new NavigationPage(new TodoListPage());
			nav.BarBackgroundColor = (Color)App.Current.Resources["primaryGreen"];
			nav.BarTextColor = Color.White;

			MainPage = nav;
		}

		public static VivamosDatabase Database
		{
			get
			{
				if (database == null)
				{
					var databasePath = DependencyService.Get<IFileHelper>().GetLocalFilePath("TodoSQLite.db");
					Debug.WriteLine("databasePath: " + databasePath);
					database = VivamosDatabase.Create(databasePath);
				}
				return database;
			}
		}


		protected override void OnStart()
		{
		}

		protected override void OnSleep()
		{
		}

		protected override void OnResume()
		{
		}
	}
}


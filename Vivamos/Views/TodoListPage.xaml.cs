﻿using System;
using System.Diagnostics;
using Vivamos.LocalDatabase.Repositories;
using Xamarin.Forms;

namespace Vivamos
{
	public partial class TodoListPage : ContentPage
	{
        private readonly TodoItemsRepository _todoItemsRepository;

        public TodoListPage()
		{
			InitializeComponent();

            _todoItemsRepository = new TodoItemsRepository(App.Database);
		}

		protected override async void OnAppearing()
		{
		}

		async void OnItemAdded(object sender, EventArgs e)
		{
		}

		async void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
		}
	}
}

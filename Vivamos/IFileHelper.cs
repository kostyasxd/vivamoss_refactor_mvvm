﻿namespace Vivamos
{
	public interface IFileHelper
	{
		string GetLocalFilePath(string filename);
	}
}

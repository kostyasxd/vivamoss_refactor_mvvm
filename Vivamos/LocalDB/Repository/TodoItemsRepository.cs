﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vivamos.LocalDatabase.Repositories
{
    public class TodoItemsRepository
    {
        private readonly VivamosDatabase _context;

        public TodoItemsRepository(VivamosDatabase context)
        {
            _context = context;
        }

        public async Task<List<TodoItem>> GetItemsAsync()
        {
            return await _context.TodoItems.ToListAsync();
        }

        public async Task<List<TodoItem>> GetItemsNotDoneAsync()
        {
            return await _context.TodoItems.Where(item => item.Done).ToListAsync();
        }

        public async Task<TodoItem> GetItemAsync(int id)
        {
            return await _context.TodoItems.SingleAsync(item => item.ID == id);
        }

        public async Task<int> SaveItemAsync(TodoItem item)
        {
            if (item.ID == 0)
            {
                await _context.TodoItems.AddAsync(item);
            }
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteItemAsync(TodoItem item)
        {
            _context.TodoItems.Remove(item);
            return await _context.SaveChangesAsync();
        }
    }
}

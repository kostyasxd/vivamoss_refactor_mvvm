﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore.Design;

namespace Vivamos
{
    public class VivamosDatabase : DbContext
    {
        #region DbSets
        public DbSet<TodoItem> TodoItems { get; set; }
        #endregion

        public static VivamosDatabase Create(string databasePath)
        {
            var dbContext = new VivamosDatabase(databasePath);
            //dbContext.Database.Migrate();
            dbContext.Database.EnsureCreated();
            return dbContext;
        }

        #region Private implementation

        public string DatabasePath { get; set; }

        public VivamosDatabase(string databasePath)
        {
            DatabasePath = databasePath;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Filename={DatabasePath}");
        }

        #endregion
    }
}

EF Migration can be created only from executable project, and not from netstandard project. This is workaround:

1. Move LocalDB folder into MigrationsHost project. 
2. Delete constructor from VivamosDatabase class (DbContext).
3. Open CMD.exe promt in Vivamos.MigrationsHost project folder.

4. Run:
	dotnet restore
	dotnet ef migrations add <MigrationName>

5. Replace migrations folder in Vivamos project.
6. Delete Vivamos.MigrationsHost LocalDB folder
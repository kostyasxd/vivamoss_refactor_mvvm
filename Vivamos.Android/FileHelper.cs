﻿using System;
using System.IO;
using Xamarin.Forms;
using Vivamos.Droid;
using Vivamos;

[assembly: Dependency(typeof(FileHelper))]
namespace Vivamos.Droid
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
    }
}
